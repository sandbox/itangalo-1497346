<?php
/**
 * @file
 * learn_topics.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function learn_topics_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function learn_topics_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function learn_topics_flag_default_flags() {
  $flags = array();
  // Exported flag: "Topic proficiency".
  $flags['learn_proficiency'] = array(
    'content_type' => 'node',
    'title' => 'Topic proficiency',
    'global' => '0',
    'types' => array(
      0 => 'topic',
    ),
    'flag_short' => 'Mark learned',
    'flag_long' => 'Click here to note that you are proficient in this topic.',
    'flag_message' => 'Way to go!',
    'unflag_short' => 'Learned!',
    'unflag_long' => 'Click here if you are no longer proficient in this topic.',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'learn_topics',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function learn_topics_node_info() {
  $items = array(
    'topic' => array(
      'name' => t('Topic'),
      'base' => 'node_content',
      'description' => t('Topics are used to break down learning areas into managable chunks. A topic may be a part of another topic, or require that another topic is learnt first. Depending on the site setup, topics may also have lessons or other learning resources.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
